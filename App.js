import React, {Component} from 'react';
import {AppStack} from "./app/config/Router";

export default class App extends Component {
    render() {
        return (
            <AppStack/>
        );
    }
}