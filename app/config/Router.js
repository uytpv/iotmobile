import Welcome from '../screens/Welcome';
import Login from '../screens/Login';
import Register from '../screens/Register';
import MyDevice from '../screens/MyDevice';
import AddDevice from '../screens/AddDevice';
import {StackNavigator} from 'react-navigation';

export const AppStack = StackNavigator(
    {
        WelcomeScreen: {
            screen: Welcome,
            navigationOptions: {
                header: null
            }
        },
        LoginScreen: {
            screen: Login,
            navigationOptions: {
                title: 'Login',
                headerLeft: null,
            }
        },
        RegisterScreen: {
            screen: Register,
            navigationOptions: {
                title: 'Register',
                headerLeft: null,
            }
        },
        MyDeviceScreen: {
            screen: MyDevice,
            navigationOptions: {
                title: 'My Device(s)',
                headerLeft: null,
            }
        },
        AddDeviceScreen: {
            screen: AddDevice,
            navigationOptions: {
                title: 'Add New Device'
            }
        },
    },
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#303131',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        },
    }
);
