import React, { Component } from 'react'
import { Text, View } from 'react-native'
import ViewContainer from '../components/ViewContainer'
import Form from '../components/Form'
import Input from '../components/Input'
import Button from '../components/Button'
import LocalStorage from '../components/LocalStorage'
import axios from 'axios'
import Toast, { DURATION } from 'react-native-easy-toast'

export default class AddDevice extends Component {
  constructor () {
    super()
    this.state = {
      device_name: '',
      device_id: '',
      user_id: 0
    }
  }

  render () {
    return (
      <ViewContainer>
        <Form>
          <Input onChangeText={(text) => this._setValue(text, 'device_name')} label="Device Name"/>
          <Input onChangeText={(text) => this._setValue(text, 'device_id')} label="Device ID"/>
          <Text
            style={{color: '#b3b3b3', fontSize: 13, marginTop: 15, alignSelf: 'center'}}>
            Look for the ID on the device "eg. esp8266"
          </Text>

          <Button onPress={() => this._addDevice()}>ADD</Button>
          <Text
            style={{color: '#b3b3b3', fontSize: 13, marginTop: 15, alignSelf: 'center', textDecorationLine: 'underline'}}>
            Learn more, how to add an device
          </Text>
        </Form>
        <Toast ref='toast'
               position="bottom"
               textStyle={{color: 'white'}}
               style={{backgroundColor: '#ba3a47'}}
        />
      </ViewContainer>
    )
  }

  _addDevice () {
    let device = {}
    device.device_name = this.state.device_name
    device.device_id = this.state.device_id
    device.user_id = this.state.user_id

    var url = 'http://localhost:8000/api/addDevice'
    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
      }
    }

    axios.post(url, JSON.stringify(device), axiosConfig)
      .then((res) => {
        if (res.status == 200) {
          this.props.navigation.navigate('Add device successful')
          this.props.navigation.navigate('MyDeviceScreen')
        }
      })
      .catch((error) => {
        var errMsg
        if (error.response) {
          errMsg = error.response.data.message

        } else if (error.request) {
          console.log(error.request)
          errMsg = 'Could not connect to the server.'
        } else {
          console.log('Error', error.message)
          errMsg = error.message
        }
        this.refs.toast.show(errMsg, DURATION.LENGTH_LONG)
      })
  }

  _setValue (text, field) {
    if (field == 'device_name') {
      this.setState({device_name: text})
      LocalStorage.get('userId').then((data) => {
        this.setState({user_id: data})
      })
    }
    if (field == 'device_id') {
      this.setState({device_id: text})
    }
  }
}