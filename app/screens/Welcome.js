import React, { Component } from 'react'
import { Text, View } from 'react-native'
import ViewContainer from '../components/ViewContainer'
import Button from '../components/Button'
import LocalStorage from '../components/LocalStorage'

export default class Welcome extends Component {
  state: {
    'userId': 0
  }

  componentWillMount () {
    LocalStorage.get('userId').then((data) => {
      this.setState({'userId': data})
    })
  }

  render () {
    return (
      <ViewContainer>
        <View style={{justifyContent: 'center', alignItems: 'center', flexGrow: 1}}>
          <Text style={{color: '#b3b3b3', fontSize: 20, marginBottom: 5}}>Welcome to</Text>
          <Text style={{color: '#ba3a47', fontSize: 30, marginBottom: 7}}>Light Control System</Text>
          <Text style={{color: '#b3b3b3', fontSize: 13, marginBottom: 7}}>uytpvgt60819@fpt.edu.vn</Text>
          <Button onPress={() => {
            this._navigateIfLoggedIn()
          }}>GO NOW!</Button>
          <Text visible={false} style={{color: '#b3b3b3', fontSize: 13, marginTop: 15, alignSelf: 'center'}}>Log out &nbsp;
            <Text style={{textDecorationLine: 'underline'}}
                  onPress={() => this._logout()}>Click Here</Text>
          </Text>
        </View>
      </ViewContainer>
    )
  }

  _navigateIfLoggedIn () {
    // console.log(this.state.userId);
    if (this.state.userId != null) {
      this.props.navigation.navigate('MyDeviceScreen')
    } else {
      this.props.navigation.navigate('LoginScreen')
    }
  }

  _logout () {
    this.props.navigation.navigate('LoginScreen')
  }
}