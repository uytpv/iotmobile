import React, { Component } from 'react'
import { AsyncStorage, KeyboardAvoidingView, StatusBar, Text, TextInput, View } from 'react-native'
import ViewContainer from '../components/ViewContainer'
import Input from '../components/Input'
import Button from '../components/Button'
import Form from '../components/Form'
import axios from 'axios'
import Toast, { DURATION } from 'react-native-easy-toast'
import LocalStorage from '../components/LocalStorage'

export default class Login extends Component {
  constructor () {
    super()
    this.state = {
      error: 'There are no error to show',
      username: '',
      password: '',
    }
  }

  render () {
    return (
      <ViewContainer>
        <Form>
          <Input onChangeText={(text) => this.validate(text, 'username')} label="Email"/>
          <Input onChangeText={(text) => this.validate(text, 'password')} label="Password" secureTextEntry={true}/>
          <Button onPress={() => this._login()}>LOGIN</Button>
          <Text style={{color: '#b3b3b3', fontSize: 13, marginTop: 15, alignSelf: 'center'}}>Don't have an account?&nbsp;
            <Text style={{textDecorationLine: 'underline'}}
                  onPress={() => this.props.navigation.navigate('RegisterScreen')}>Register</Text>
          </Text>
        </Form>
        <Toast ref='toast'
               position="bottom"
               textStyle={{color: 'white'}}
               style={{backgroundColor: '#ba3a47'}}
        />
      </ViewContainer>
    )
  }

  validate (text, field) {
    if (field == 'username') {
      this.setState({
        username: text,
      })
    }
    if (field == 'password') {
      this.setState({
        password: text
      })
    }
  }

  _login () {
    let loginUser = {}
    loginUser.username = this.state.username
    loginUser.password = this.state.password

    var url = 'http://localhost:8000/api/login'
    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
      }
    }

    axios.post(url, JSON.stringify(loginUser), axiosConfig)
      .then((res) => {
        if (res.status == 200) {
          if (res.data.statusCode == 1) {
            LocalStorage.save('userId', res.data.userId)
            this.props.navigation.navigate('MyDeviceScreen')
          } else {
            this.refs.toast.show(res.data.message, DURATION.LENGTH_LONG)
          }
        }
      })
      .catch((error) => {
        var errMsg
        if (error.response) {
          errMsg = error.response.data.message

        } else if (error.request) {
          console.log(error.request)
          errMsg = 'Could not connect to the server.'
        } else {
          console.log('Error', error.message)
          errMsg = error.message
        }
        this.refs.toast.show(errMsg, DURATION.LENGTH_LONG)
      })
  }
}
