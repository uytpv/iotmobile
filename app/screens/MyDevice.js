import React, { Component } from 'react'
import { AsyncStorage, Image, ScrollView, Text, TouchableOpacity, View } from 'react-native'
import LocalStorage from '../components/LocalStorage'
import axios from 'axios'
import Toast, { DURATION } from 'react-native-easy-toast'

export default class MyDevice extends Component {
  state = {
    devices: [],
    buttonText: 'ON'
  }

  componentWillMount () {
    LocalStorage.get('userId').then((data) => {
      this._fetchData(data)
    })
  }

  _fetchData (userId) {
    var url = 'http://localhost:8000/api/' + userId + '/devices'
    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
      }
    }
    axios.get(url, axiosConfig)
      .then((res) => {
        if (res.status === 200) {
          this.setState({devices: res.data.devices})
        }
      })
      .catch((error) => {
        var errMsg
        if (error.response) {
          errMsg = error.response.data.message

        } else if (error.request) {
          console.log(error.request)
          errMsg = 'Could not connect to the server.'
        } else {
          console.log('Error', error.message)
          errMsg = error.message
        }
        this.refs.toast.show(errMsg, DURATION.LENGTH_LONG)
      })
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <ScrollView style={{backgroundColor: '#292a2a'}}>
          {
            this.state.devices.map((item, index) => (
              <View key={item.id} style={styles.item}>
                <Image style={{width: 30, height: 30}} source={require('../img/ico-light.png')}/>
                <Text style={{color: '#fff', fontSize: 20,}}>{item.device_name}</Text>
                <TouchableOpacity
                  style={item.status == 0 ? styles.switchStyle : styles.switchONStyle}
                  onPress={() => this._switchTheLight(item.id, item.status == 0 ? 1 : 0)}>
                  <Text style={item.status == 0 ? styles.switchTextStyle : styles.switchTextONStyle}>
                    {item.status == 0 ? 'ON' : 'OFF'}</Text>
                </TouchableOpacity>
              </View>
            ))
          }
        </ScrollView>
        <TouchableOpacity
          style={{height: 80, backgroundColor: '#303131', justifyContent: 'flex-end'}}
          onPress={() => this.props.navigation.navigate('AddDeviceScreen')}
        >
          <Text style={{paddingTop: 10, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', color: '#ba3a47', fontSize: 80}}>+</Text>
        </TouchableOpacity>
        <Toast ref='toast'
               position="bottom"
               textStyle={{color: 'white'}}
               style={{backgroundColor: '#ba3a47'}}
        />
      </View>
    )
  }

  _switchTheLight (device_id, action) {
    var url = 'http://localhost:8000/api/turnTheLight/' + device_id + '/' + action
    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
      }
    }
    axios.get(url, axiosConfig)
      .then((res) => {
        if (res.status === 200) {
          this.componentWillMount()
          this.refs.toast.show(res.data.message, DURATION.LENGTH_LONG)
        } else {
          this.refs.toast.show(res.data.message, DURATION.LENGTH_LONG)
        }
      })
      .catch((error) => {
        var errMsg
        if (error.response) {
          errMsg = error.response.data.message
        } else if (error.request) {
          console.log(error.request)
          errMsg = 'Could not connect to the server.'
        } else {
          console.log('Error', error.message)
          errMsg = error.message
        }
        this.refs.toast.show(errMsg, DURATION.LENGTH_LONG)
      })
  }
}
const styles = {
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
    borderColor: '#db4453',
    borderBottomWidth: 1,
    backgroundColor: '#ba3a47',
  },
  switchTextStyle: {
    paddingTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    color: '#fff'
  },
  switchStyle: {
    borderColor: '#fff',
    borderWidth: 1,
    width: 50,
    height: 50,
    borderRadius: 50
  },
  switchTextONStyle: {
    paddingTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    color: '#ba3a47'
  },
  switchONStyle: {
    borderColor: '#ba3a47',
    backgroundColor: '#fff',
    borderWidth: 1,
    width: 50,
    height: 50,
    borderRadius: 50
  },
}