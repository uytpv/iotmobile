import React, { Component } from 'react'
import { Headers, Text, View } from 'react-native'
import ViewContainer from '../components/ViewContainer'
import Form from '../components/Form'
import Button from '../components/Button'
import Input from '../components/Input'
import axios from 'axios'
import Toast, { DURATION } from 'react-native-easy-toast'

export default class Register extends Component {
  constructor () {
    super()
    this.state = {
      error: 'Hello there is no error',
      name: '',
      email: '',
      password: '',
      password_confirmation: ''
    }
  }

  _register () {
    let registerUser = {}
    registerUser.name = this.state.name
    registerUser.email = this.state.email
    registerUser.password = this.state.password
    registerUser.password_confirmation = this.state.password_confirmation

    var url = 'http://localhost:8000/api/register'
    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
      }
    }

    axios.post(url, JSON.stringify(registerUser), axiosConfig)
      .then((res) => {
        if (res.status == 200) {
          this.props.navigation.navigate('LoginScreen')
        }
      })
      .catch((error) => {
        var errMsg
        if (error.response) {
          errMsg = error.response.data.message

        } else if (error.request) {
          console.log(error.request)
          errMsg = 'Could not connect to the server.'
        } else {
          console.log('Error', error.message)
          errMsg = error.message
        }
        this.refs.toast.show(errMsg, DURATION.LENGTH_LONG)
      })
  }

  render () {
    return (
      <ViewContainer>
        <Form>
          <Input onChangeText={(text) => this.validate(text, 'email')} label="Email"/>
          <Input onChangeText={(text) => this.validate(text, 'password')} secureTextEntry={true} label="Password"/>
          <Input onChangeText={(text) => this.validate(text, 'password_confirmation')} secureTextEntry={true} label="Retype - Password"/>
          <Button onPress={() => this._register()}>REGISTER</Button>
          <Text style={{color: '#b3b3b3', fontSize: 13, marginTop: 15, alignSelf: 'center'}}>Already have an account?&nbsp;
            <Text style={{textDecorationLine: 'underline'}}
                  onPress={() => this.props.navigation.navigate('LoginScreen')}>Login</Text>
          </Text>
        </Form>
        <Toast ref='toast'
               position="bottom"
               textStyle={{color: 'white'}}
               style={{backgroundColor: '#ba3a47'}}
        />
      </ViewContainer>
    )
  }

  validate (text, field) {
    if (field == 'email') {
      this.setState({
        email: text,
        name: text
      })
    }
    if (field == 'password') {
      this.setState({
        password: text
      })
    }
    if (field == 'password_confirmation') {
      this.setState({
        password_confirmation: text
      })
    }
  }
}