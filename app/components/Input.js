import React from 'react';
import {Text, TextInput, View} from 'react-native';

const Input = ({label, value, placeholder, secureTextEntry, onChangeText}) => {
    return (
        <View>
            <Text style={styles.labelStyle}>{label}</Text>
            <TextInput style={styles.inputStyle} onChangeText={onChangeText} secureTextEntry={secureTextEntry}/>
        </View>
    );
};
const styles = {
    inputStyle: {
        color: '#b3b3b3',
        backgroundColor: '#303131',
        padding: 10,
        fontSize: 16,
        lineHeight: 23,
    },
    labelStyle: {
        color: '#b3b3b3',
        fontSize: 16,
        marginBottom: 10,
        marginTop: 10,
    },

};
export default Input;