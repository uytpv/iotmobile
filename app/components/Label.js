import React from 'react'
import {Dimensions, Text, TouchableOpacity} from 'react-native';

const Label = ({onPress, children}) => {
    return (
        <Text style={styles.labelStyle}>{children}</Text>
    );
};

const styles = {
    labelStyle: {
        color: '#b3b3b3',
        fontSize: 13,
        paddingLeft: 20,
        flex: 1,
    },
};

export default Label;