import React from 'react';
import {KeyboardAvoidingView, View} from 'react-native';

const Form = (props) => {
    return (
        <KeyboardAvoidingView behavior="padding" style={{justifyContent: 'flex-start', alignItems: 'stretch', flexGrow: 1, marginTop: 20}}>
            {props.children}
        </KeyboardAvoidingView>
    );
};

export default Form;