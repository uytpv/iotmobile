import React from 'react';
import {StatusBar, View} from 'react-native';

const ViewContainer = (props) => {
    return (
        <View style={{flexDirection: 'column', justifyContent: 'space-between', flex: 1, backgroundColor: '#292a2a', paddingRight: 20, paddingLeft: 20}}>
            <StatusBar backgroundColor='blue' barStyle='light-content' />
            {props.children}
        </View>);
};
export default ViewContainer;